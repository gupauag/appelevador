package br.com.itau;

public class Elevador {
    public int[] andar;
    public int[] capacidade; //qnt de pessoas (nao considero o pessoas

    public Elevador(int capacidade, int totalAndares){
        this.andar = new int[totalAndares];
        this.capacidade = new int[capacidade];
    }

    public int getAndarAtivo() {
        int andarAtivo = 0;
        for (int i = 0; i < this.andar.length; i++){
            if(this.andar[i] == 1){
                andarAtivo = i;
                i = andar.length-1;
            }
        }
        if(andarAtivo==0) return 0;
        else return andarAtivo+1;
    }
    public int getCapatidadeAtiva() {
        int capacidadeAtiva = 0;
        for (int i = 0; i < this.capacidade.length; i++){
            if(this.capacidade[i] == 1){
                capacidadeAtiva++;
            }
        }
        return capacidadeAtiva;
    }

    public void setAndar(int andar) {
        for (int i = 0; i < this.andar.length; i++){
            this.andar[i] = 0;
        }
        if(andar==0) this.andar[andar] = 1;
        //else if(andar == 10) this.andar[andar-1] = 1;
        else this.andar[andar - 1] = 1;

    }


    public void setCapacidade(int[] capacidade) {
        this.capacidade = capacidade;
    }

    private int retornaDisponibilidade(){
        int countDisponivel = 0;
        for (int i = 0; i < capacidade.length; i++){
            if(capacidade[i]==0){
                countDisponivel++;
            }
        }
        return countDisponivel;
    }

    public void entra(int pessoas){
        int contPessoas = 1;
        if(retornaDisponibilidade() > pessoas){
            for (int i = 0; i < capacidade.length; i++){
                if(capacidade[i]==0 && contPessoas <= pessoas){
                    capacidade[i] = 1;
                    contPessoas++;
                }
            }
        }else System.out.println("Não é permitido entrar mais que "+retornaDisponibilidade()+" pessoas. Escolha quem entrará");
    }

    public void sai(){ //remonve somente 1 pessoa
        boolean saiu = false;
        if(retornaDisponibilidade() == capacidade.length) System.out.println("Não há mais pessoas para sair do elevavor");
        else{
            for (int i = 0; i < capacidade.length; i++){
                if(capacidade[i] == 1){
                    capacidade[i] = 0;
                    i = capacidade.length-1;
                }
            }
        }
    }

    public void sobe(int andar){
        if(this.andar[this.andar.length-1] == 1) System.out.println("O elevador já está no ultimo andar. Função não disponível");
        else if(andar == 0) System.out.println("Vc já esta no térreo!");
        else setAndar(andar);
    }

    public void desce(int andar){
        if(this.andar[0] == 1) System.out.println("O elevador já está no térreo. Função não disponível");
        else if(andar == this.andar.length+1) System.out.println("Vc já esta no ultimo andar!");
        else setAndar(andar);
    }

    public void imprimeElevador(){
        System.out.println("Elevador no Andar: "+getAndarAtivo());
        System.out.println("Capacidade atual: "+getCapatidadeAtiva());
        System.out.println("----------------------------------------");
    }



}
