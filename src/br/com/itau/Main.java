package br.com.itau;

public class Main {

    public static void main(String[] args) {
        Elevador elevador = new Elevador(7,10);

        System.out.println("Elevador com "+elevador.andar.length+" andares");
        System.out.println("Elevador com capacidade de "+elevador.capacidade.length+" pessoas\n");

        elevador.imprimeElevador();

        elevador.entra(5);
        elevador.imprimeElevador();

        elevador.sai();
        elevador.imprimeElevador();

        elevador.sobe(10);
        elevador.imprimeElevador();

        elevador.sobe(10);
        elevador.imprimeElevador();

        elevador.desce(4);
        elevador.imprimeElevador();

        elevador.sai();
        elevador.imprimeElevador();

        elevador.sai();
        elevador.imprimeElevador();

        elevador.sai();
        elevador.imprimeElevador();

        elevador.entra(10);
        elevador.imprimeElevador();

        elevador.entra(2);
        elevador.imprimeElevador();

        elevador.desce(0);
        elevador.imprimeElevador();

        elevador.sai();
        elevador.imprimeElevador();
        elevador.sai();
        elevador.imprimeElevador();
        elevador.sai();
        elevador.imprimeElevador();
        elevador.sai();
        elevador.imprimeElevador();




    }
}
